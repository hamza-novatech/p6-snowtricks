<?php

namespace App\Form;

use App\Entity\Tricks;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class TricksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nom du trick'])
            ->add('description', TextareaType::class, ['label' => 'Description du trick'])
            ->add('tricksGroup', EntityType::class, [
                'class'        => 'App:TricksGroup',
                'choice_label' => 'name',
                'label'        => 'Choississez une ou plusieurs famille de tricks',
                'expanded'     => true,
                'multiple'     => true,
            ])
            ->add('tricksVideos', CollectionType::class, [
                'entry_type' => TricksAssetVideoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ])
            ->add('photos', FileType::class, [
                'label' => 'Ajouter des photos',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
            ])
            ->add('tricksPhotos', CollectionType::class, [
                'entry_type' => TricksAssetPhotoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tricks::class,
        ]);
    }
}
