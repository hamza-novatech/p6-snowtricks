<?php

namespace App\DataFixtures;

use App\Entity\TricksGroup;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TricksGroupFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        //Create Tricks Group
        $this->fixtureTricksGroup('grab', $manager);
        $this->fixtureTricksGroup('rotations', $manager);
        $this->fixtureTricksGroup('flips', $manager);
        $this->fixtureTricksGroup('rotations désaxées', $manager);
        $this->fixtureTricksGroup('slides', $manager);
    }
    
    /**
     * fixtureTricksGroup
     *
     * @param  string $tricksGroupName
     * @param  objectManager $manager
     * @return void
     */
    protected function fixtureTricksGroup(String $tricksGroupName, objectManager $manager)
    {
        $faker = Factory::create();
        $tricksGroup = new TricksGroup();
        $tricksGroup->setName($tricksGroupName)->setDescription($faker->text($maxNbChars = 200))->setCreatedAt(new DateTime());
        $manager->persist($tricksGroup);
        $manager->flush();
        $this->setReference($tricksGroupName, $tricksGroup);
    }

    public function getOrder()
    {
        return 1;
    }
}
