<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Entity\Tricks;
use App\Entity\TricksAsset;
use App\Entity\TricksPhoto;
use App\Entity\TricksVideo;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Youtube;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }
    
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $faker->addProvider(new Youtube($faker));

        // create 10 users | 10 tricks with 2 assets and 1 group | 10 messages
        for ($i = 0; $i < 10; $i++) {
            //random tricksGroup
            $tricksGroupArray = ['grab', 'rotations', 'flips', 'rotations désaxées', 'slides'];
            $randomGroup = array_rand($tricksGroupArray, 1);
            $tricksGroup = $this->getReference($tricksGroupArray[$randomGroup]);

            //Create 10 Users
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($faker->password);
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->LastName);
            $user->setRegisterOn(new DateTime());
            $user->setAvatar('http://lorempixel.com/92/92/people/' . random_int(1, 10));

            //Create 10 Tricks
            $tricks = new Tricks();
            $tricks->setName($faker->word() . ' ' . $faker->word() . ' ' . $faker->word());
            $tricks->setDescription($faker->text);
            $tricks->setCreateAt(new DateTime());
            $tricks->setUpdateAt(new DateTime());
            $tricks->setSlug($this->slugger->slug($tricks->getName()));
            $tricks->addTricksGroup($tricksGroup);
            $tricks->setUser($user);

            //Create 10 asset video
            $assetVideo = new TricksVideo();
            $assetVideo->setAsset('https://www.youtube.com/watch?v=UGdif-dwu-8');
            $assetVideo->setTrick($tricks);

            // Create 10 asset main photo
            $assetMainPhoto = new TricksPhoto();
            $assetMainPhoto->setAsset('tricks-default.jpg');
            $assetMainPhoto->setType('main');
            $assetMainPhoto->setTrick($tricks);
            
            //Create 10 asset photo
            $assetPhoto = new TricksPhoto();
            $assetPhoto->setAsset('tricks-default.jpg');
            $assetPhoto->setType('photo');
            $assetPhoto->setTrick($tricks);

            //
            $message = new Message();
            $message->setMessage($faker->text);
            $message->setCreateAt(new DateTime());
            $message->setUpdateAt(new DateTime());
            $message->setUser($user);
            $message->setTricks($tricks);

            $manager->persist($assetMainPhoto);
            $manager->persist($assetPhoto);
            $manager->persist($assetVideo);
            $manager->persist($message);
            $manager->persist($tricks);
            $manager->persist($user);
        }

        //For testing with PHPunit
        $tricks = new Tricks();
        $tricks->setName('Trick de test');
        $tricks->setDescription($faker->text);
        $tricks->setCreateAt(new DateTime());
        $tricks->setUpdateAt(new DateTime());
        $tricks->setSlug('just-a-fake-slug');
        $tricks->addTricksGroup($tricksGroup);
        $tricks->setUser($user);

        $assetMainPhoto = new TricksPhoto();
        $assetMainPhoto->setAsset('tricks-default.jpg');
        $assetMainPhoto->setType('main');
        $assetMainPhoto->setTrick($tricks);

        $assetVideo = new TricksVideo();
        $assetVideo->setAsset('https://www.youtube.com/watch?v=SQyTWk7OxSI');
        $assetVideo->setTrick($tricks);

        $assetPhoto = new TricksPhoto();
        $assetPhoto->setAsset('tricks-default.jpg');
        $assetPhoto->setType('photo');
        $assetPhoto->setTrick($tricks);

        $message = new Message();
        $message->setMessage('message de test');
        $message->setCreateAt(new DateTime());
        $message->setUpdateAt(new DateTime());
        $message->setUser($user);
        $message->setTricks($tricks);

        $manager->persist($assetPhoto);
        $manager->persist($assetMainPhoto);
        $manager->persist($assetVideo);
        $manager->persist($tricks);
        $manager->persist($message);

        //End Tests

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
