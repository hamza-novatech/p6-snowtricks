<?php

namespace App\Service;

use App\Entity\Tricks;
use App\Entity\TricksAsset;
use App\Entity\TricksPhoto;
use App\Entity\TricksVideo;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class TricksAssetService
{
    private $em;
    private $flashBagInterface;
    private $fileUploader;

    public function __construct(EntityManagerInterface $em, FlashBagInterface $flashBagInterface, FileUploader $fileUploader)
    {
        $this->em = $em;
        $this->flashBagInterface = $flashBagInterface;
        $this->fileUploader = $fileUploader;
    }

    public function deletePhoto(TricksPhoto $tricksAsset): void
    {
        $this->em->remove($tricksAsset);
        $this->em->flush();

        $this->flashBagInterface->add(
            'success',
            'Le média est bien supprimé !'
        );
    }

    public function deleteVideo(TricksVideo $tricksAsset): void
    {
        $this->em->remove($tricksAsset);
        $this->em->flush();

        $this->flashBagInterface->add(
            'success',
            'Le média est bien supprimé !'
        );
    }

    public function addVideo(TricksVideo $tricksAsset): void
    {
        $this->em->persist($tricksAsset);
        $this->em->flush($tricksAsset);

        $this->flashBagInterface->add(
            'success',
            'La vidéo est bien ajoutée !'
        );
    }

    public function newVideo(Tricks $tricks)
    {
        $asset = new TricksVideo;
        $asset->setTrick($tricks);

        return $asset;
    }

    public function ifDailymotion(TricksVideo $tricksAsset)
    {
        if (strpos($tricksAsset->getAsset(), 'dailymotion')) {
            $videoId = str_replace('https://www.dailymotion.com/video/', '', $tricksAsset->getAsset());
            $tricksAsset->setAsset('https://www.dailymotion.com/embed/video/' . $videoId);

            return $tricksAsset;
        }
    }

    public function newPhoto(Tricks $tricks, String $type)
    {
        $asset = new TricksPhoto;
        $asset->setTrick($tricks);
        $asset->setType($type);

        return $asset;
    }

    public function addPhoto(TricksPhoto $tricksPhoto, Form $form)
    {
        /** @var UploadedFile $brochureFile */
        $assetPhotoFile = $form->get('asset')->getData();
        if ($assetPhotoFile) {
            $assetPhotoFileName = $this->fileUploader->upload($assetPhotoFile);
            $tricksPhoto->setAsset($assetPhotoFileName);
        }

        $this->flashBagInterface->add(
            'success',
            'Le photo est bien ajoutée !'
        );

        $this->em->persist($tricksPhoto);
        $this->em->flush($tricksPhoto);
    }
}
