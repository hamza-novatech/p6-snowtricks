<?php

namespace App\Service;

use App\Entity\Tricks;
use App\Repository\MessageRepository;
use App\Repository\TricksAssetRepository;
use App\Repository\TricksRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class TricksService
{
    private $tricksRepository;
    private $em;
    private $messageRepository;
    private $tricksAssetRepository;
    private $flashBagInterface;
    private $slugger;
    private $security;

    public function __construct(
        TricksRepository $tricksRepository,
        MessageRepository $messageRepository,
        TricksAssetRepository $tricksAssetRepository,
        EntityManagerInterface $em,
        FlashBagInterface $flashBagInterface,
        SluggerInterface $slugger,
        Security $security
    ) {
        $this->tricksRepository = $tricksRepository;
        $this->em = $em;
        $this->messageRepository = $messageRepository;
        $this->tricksAssetRepository = $tricksAssetRepository;
        $this->flashBagInterface = $flashBagInterface;
        $this->slugger = $slugger;
        $this->security = $security;
    }

    public function addTricks(Tricks $trick): void
    {
        $this->flashBagInterface->add(
            'success',
            'Le tricks est bien créé !'
        );

        $trick->setSlug($this->slugger->slug($trick->getName()));

        $this->em->persist($trick);
        $this->em->flush();
    }

    public function editTricks(Tricks $trick): void
    {
        $this->flashBagInterface->add(
            'success',
            'Le tricks est bien modifié !'
        );

        $trick->setSlug($this->slugger->slug($trick->getName()));
        
        $this->em->persist($trick);
        $this->em->flush();
    }

    public function deleteTricks(Tricks $trick): void
    {
        $todelete = $this->messageRepository->findBy(['tricks' => $trick->getId() ]);
        foreach ($todelete as $value) {
            $this->em->remove($value);
        }

        $todelete = $this->tricksAssetRepository->findBy(['tricks' => $trick->getId() ]);
        foreach ($todelete as $value) {
            $this->em->remove($value);
        }

        $todelete = $this->tricksRepository->find($trick);
        $this->em->remove($todelete);
        $this->em->flush();

        $this->flashBagInterface->add(
            'success',
            'Le tricks est bien supprimé !
        '
        );
    }

    public function new()
    {
        $trick = new Tricks;
        $trick->setCreateAt(new DateTime());
        $trick->setUpdateAt(new DateTime());
        
        $user = $this->security->getUser();
        $trick->setUser($user);

        return $trick;
    }
}
