<?php

namespace App\Service;

use App\Entity\Message;
use App\Entity\Tricks;
use App\Repository\MessageRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;

class MessagesService
{
    private $em;
    private $messageRepository;
    private $flashBagInterface;
    private $security;

    public function __construct(MessageRepository $messageRepository, EntityManagerInterface $em, FlashBagInterface $flashBagInterface, Security $security)
    {
        $this->em = $em;
        $this->messageRepository = $messageRepository;
        $this->flashBagInterface = $flashBagInterface;
        $this->security = $security;
    }

    public function deleteMessages(Message $message): void
    {
        $this->em->remove($message);
        $this->em->flush();
        $this->flashBagInterface->add(
            'success',
            'Le commentaire est bien supprimé'
        );
    }

    public function new(Tricks $trick): Message
    {
        $message = new Message();
        $message->setTricks($trick);
        $message->setCreateAt(new DateTime());
        $message->setUpdateAt(new DateTime());

        $message->setUser($this->security->getUser());

        return $message;
    }

    public function add(Message $message): void
    {
        $this->em->persist($message);
        $this->em->flush();
        $this->flashBagInterface->add(
            'success',
            'Votre message est bien envoyé!'
        );
    }
}
