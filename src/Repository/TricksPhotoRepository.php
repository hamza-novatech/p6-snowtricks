<?php

namespace App\Repository;

use App\Entity\TricksPhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TricksPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method TricksPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method TricksPhoto[]    findAll()
 * @method TricksPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TricksPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TricksPhoto::class);
    }

    // /**
    //  * @return TricksPhoto[] Returns an array of TricksPhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TricksPhoto
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
