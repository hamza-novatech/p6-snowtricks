<?php

namespace App\Repository;

use App\Entity\TricksAsset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TricksAsset|null find($id, $lockMode = null, $lockVersion = null)
 * @method TricksAsset|null findOneBy(array $criteria, array $orderBy = null)
 * @method TricksAsset[]    findAll()
 * @method TricksAsset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TricksAssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TricksAsset::class);
    }

    // /**
    //  * @return TricksAsset[] Returns an array of TricksAsset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TricksAsset
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
