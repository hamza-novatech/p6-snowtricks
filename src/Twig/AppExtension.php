<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @codeCoverageIgnore
     */
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            // new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    
    /**
     * @codeCoverageIgnore
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('videoThumb', [$this, 'videoThumb']),
            new TwigFunction('gravatar', [$this, 'gravatar']),
        ];
    }
    
    /**
     * videoThumb
     * Return a Thumbnail from Youtube, Vimeo or Dailymotion URL
     *
     * @param  mixed $assetUrl
     * @return void
     */
    public function videoThumb($assetUrl)
    {
        $assetUrl = str_replace('http://', 'https://', $assetUrl);
        if (strpos($assetUrl, 'youtube')) {
            $videoId = str_replace('https://www.youtube.com/watch?v=', '', $assetUrl);
            $videoThumb = 'https://i3.ytimg.com/vi/' . $videoId . '/hqdefault.jpg';
            return $videoThumb;
        }

        if (strpos($assetUrl, 'youtu.be')) {
            $videoId = str_replace('https://youtu.be/', '', $assetUrl);
            $videoThumb = 'https://i3.ytimg.com/vi/' . $videoId . '/hqdefault.jpg';
            return $videoThumb;
        }

        if (strpos($assetUrl, 'vimeo')) {
            $videoId = str_replace('https://vimeo.com/', '', $assetUrl);
            $vimeoThumbs = 'http://vimeo.com/api/v2/video/' . $videoId . '.json';

            //curl request
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $vimeoThumbs);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $curlData = curl_exec($curl);
            curl_close($curl);

            $vimeoThumbs = json_decode($curlData, true);
            $videoThumb = $vimeoThumbs[0]['thumbnail_medium'];
            $videoThumb = str_replace('http://', 'https://', $videoThumb);
            return $videoThumb;
        }
        
        if (strpos($assetUrl, 'dailymotion.com/embed')) {
            $videoId = str_replace('https://www.dailymotion.com/embed/video/', '', $assetUrl);
            $videoThumb = 'https://www.dailymotion.com/thumbnail/video/' . $videoId;
            return $videoThumb;
        }

        if (strpos($assetUrl, 'dailymotion')) {
            $videoId = str_replace('https://www.dailymotion.com/video/', '', $assetUrl);
            $videoThumb = 'https://www.dailymotion.com/thumbnail/video/' . $videoId;
            return $videoThumb;
        }
        
        $videoThumb = '/img/video-thumb.png';
        return $videoThumb;
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source https://gravatar.com/site/implement/images/php/
     */
    public function gravatar($value, $s = 40, $d = 'mp', $r = 'g', $img = false, $atts = array())
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($value)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';

            // @codeCoverageIgnoreStart
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }
            // @codeCoverageIgnoreEnd

            $url .= ' />';
        }
        return $url;
    }
}
