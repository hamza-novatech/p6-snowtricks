<?php

namespace App\Entity;

use App\Repository\TricksRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TricksRepository::class)
 */
class Tricks
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=TricksGroup::class, inversedBy="tricks")
     */
    private $tricksGroup;

    /**
     * @ORM\OneToMany(targetEntity=TricksAsset::class, mappedBy="tricks", cascade={"persist"})
     */
    private $tricksAssets;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="tricks")
     */
    private $messages;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=TricksVideo::class, mappedBy="trick", orphanRemoval=true, cascade={"persist"})
     */
    private $tricksVideos;

    /**
     * @ORM\OneToMany(targetEntity=TricksPhoto::class, mappedBy="trick", orphanRemoval=true,  cascade={"persist"})
     */
    private $tricksPhotos;

    public function __construct()
    {
        $this->tricksGroup = new ArrayCollection();
        $this->tricksAssets = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->tricksVideos = new ArrayCollection();
        $this->tricksPhotos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|TricksGroup[]
     */
    public function getTricksGroup(): Collection
    {
        return $this->tricksGroup;
    }

    public function addTricksGroup(TricksGroup $tricksGroup): self
    {
        if (!$this->tricksGroup->contains($tricksGroup)) {
            $this->tricksGroup[] = $tricksGroup;
        }

        return $this;
    }

    public function removeTricksGroup(TricksGroup $tricksGroup): self
    {
        if ($this->tricksGroup->contains($tricksGroup)) {
            $this->tricksGroup->removeElement($tricksGroup);
        }

        return $this;
    }

    /**
     * @return Collection|TricksAsset[]
     */
    public function getTricksAssets(): Collection
    {
        return $this->tricksAssets;
    }

    public function addTricksAsset(TricksAsset $tricksAsset): self
    {
        if (!$this->tricksAssets->contains($tricksAsset)) {
            $this->tricksAssets[] = $tricksAsset;
            $tricksAsset->setTricks($this);
        }

        return $this;
    }

    public function removeTricksAsset(TricksAsset $tricksAsset): self
    {
        if ($this->tricksAssets->contains($tricksAsset)) {
            $this->tricksAssets->removeElement($tricksAsset);
            // set the owning side to null (unless already changed)
            if ($tricksAsset->getTricks() === $this) {
                $tricksAsset->setTricks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setTricks($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getTricks() === $this) {
                $message->setTricks(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|TricksVideo[]
     */
    public function getTricksVideos(): Collection
    {
        return $this->tricksVideos;
    }

    public function addTricksVideo(TricksVideo $tricksVideo): self
    {
        if (!$this->tricksVideos->contains($tricksVideo)) {
            $this->tricksVideos[] = $tricksVideo;
            $tricksVideo->setTrick($this);
        }

        return $this;
    }

    public function removeTricksVideo(TricksVideo $tricksVideo): self
    {
        if ($this->tricksVideos->contains($tricksVideo)) {
            $this->tricksVideos->removeElement($tricksVideo);
            // set the owning side to null (unless already changed)
            if ($tricksVideo->getTrick() === $this) {
                $tricksVideo->setTrick(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TricksPhoto[]
     */
    public function getTricksPhotos(): Collection
    {
        return $this->tricksPhotos;
    }

    public function addTricksPhoto(TricksPhoto $tricksPhoto): self
    {
        if (!$this->tricksPhotos->contains($tricksPhoto)) {
            $this->tricksPhotos[] = $tricksPhoto;
            $tricksPhoto->setTrick($this);
        }

        return $this;
    }

    public function removeTricksPhoto(TricksPhoto $tricksPhoto): self
    {
        if ($this->tricksPhotos->contains($tricksPhoto)) {
            $this->tricksPhotos->removeElement($tricksPhoto);
            // set the owning side to null (unless already changed)
            if ($tricksPhoto->getTrick() === $this) {
                $tricksPhoto->setTrick(null);
            }
        }

        return $this;
    }
}
