<?php

namespace App\Controller;

use App\Entity\Tricks;
use App\Form\MessageType;
use App\Form\TricksType;
use App\Repository\MessageRepository;
use App\Repository\TricksPhotoRepository;
use App\Repository\TricksRepository;
use App\Service\FileUploader;
use App\Service\MessagesService;
use App\Service\TricksAssetService;
use App\Service\TricksService;
use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TricksController extends AbstractController
{
    /**
     * @Route("/{page}", defaults={"page"="1"}, name="app_home", requirements={"page"="\d+"})
     */
    public function index(TricksRepository $tricksRepository, $page): Response
    {
        $limit = 8;
        $tricks = $tricksRepository->paginateTricks($page, $limit);

        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($tricks) / $limit),
            'nomRoute' => 'app_home',
        );
        //dd($pagination);
        return $this->render('tricks/index.html.twig', [
            'tricks' => $tricks,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/trick/{slug}/{page}", defaults={"page"="1"}, name="app_show_trick", methods={"GET","POST"}, requirements={"page"="\d+"})
     */
    public function show(Tricks $trick, $page, Request $request, MessageRepository $messageRepository, MessagesService $messagesService): Response
    {
        $limit = 5;
        $messages = $messageRepository->paginateMessages($page, $limit, $trick);
        
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($messages) / $limit),
            'nomRoute' => 'app_show_trick',
            'paramsRoute' => array('slug' => $trick->getSlug()),
        );

        $message = $messagesService->new($trick);


        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $messagesService->add($message);
        }

        return $this->render('tricks/show/show.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
            'messages' => $messages,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/create", methods="GET|POST", name="app_create_trick")
     */
    public function create(Request $request, TricksService $tricksService, TricksAssetService $tricksAssetService, FileUploader $fileUploader, TricksPhotoRepository $tricksPhotoRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $trick = $tricksService->new();

        $form = $this->createForm(TricksType::class, $trick);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $videos = $form->get('tricksVideos')->getData();

            foreach ($videos as $video) {
                $video->setTrick($trick);
                $trick->addTricksVideo($video);
            }

            $photos = $form->get('photos')->getData();
            
            $main = 0;

            foreach ($photos as $photo) {
                $mainphoto = $tricksPhotoRepository->findBy(['type' => 'main', 'trick' => $trick], []);
                $filename = $fileUploader->upload($photo);
                $asset = $tricksAssetService->newPhoto($trick, 'photo');

                if ((empty($mainphoto)) && ($main == 0)) {
                    $asset->setType('main');
                }

                $asset->setAsset($filename);
                $trick->addTricksPhoto($asset);

                $main++;
            }
            
            try {
                $tricksService->addTricks($trick);

                return $this->redirectToRoute('app_show_trick', ['slug' => $trick->getSlug()]);
            } catch (DBALException $e) {
                $this->addFlash(
                    'error',
                    'Une erreur c\'est produite lors de l\'enregistrement !'
                );
                return $this->render('tricks/create/create_trick.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }

        return $this->render('tricks/create/create_trick.html.twig', [
            'form' => $form->createView(),
            'trick' => $trick,
        ]);
    }

    /**
     * @Route("/edit/{slug}", methods="GET|POST", name="app_edit_trick")
     */
    public function edit(Request $request, Tricks $trick, TricksService $tricksService, FileUploader $fileUploader, TricksAssetService $tricksAssetService, TricksPhotoRepository $tricksPhotoRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $trick->setUpdateAt(new DateTime());
        $form = $this->createForm(TricksType::class, $trick);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $videos = $form->get('tricksVideos')->getData();

            foreach ($videos as $video) {
                $video->setTrick($trick);
                $trick->addTricksVideo($video);
            }

            $photos = $form->get('photos')->getData();

            $main = 0;

            foreach ($photos as $photo) {
                $mainphoto = $tricksPhotoRepository->findBy(['type' => 'main', 'trick' => $trick], []);
                $filename = $fileUploader->upload($photo);
                $asset = $tricksAssetService->newPhoto($trick, 'photo');


                if ((empty($mainphoto)) && ($main == 0)) {
                    $asset->setType('main');
                }

                $asset->setAsset($filename);
                $trick->addTricksPhoto($asset);

                $main++;
            }

            $tricksService->editTricks($trick);

            return $this->redirectToRoute('app_show_trick', ['slug' => $trick->getSlug()]);
        }

        return $this->render('tricks/create/create_trick.html.twig', [
            'form' => $form->createView(),
            'trick' => $trick,
        ]);
    }

    /**
     * @Route("/delete/{slug}", methods="GET", name="app_delete_trick")
     */
    public function delete(Tricks $trick, TricksService $tricksService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $tricksService->deleteTricks($trick);

        return $this->redirectToRoute('app_home');
    }
}
