<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Repository\TricksRepository;
use App\Service\MessagesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MessagesController extends AbstractController
{
    /**
    * @Route("/trick/{slug}/message/delete/{id}", name="app_delete_comment")
    */
    public function delete(Message $message, MessageRepository $messageRepository, $slug, TricksRepository $tricksRepository, MessagesService $messagesService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $todelete = $messageRepository->find($message->getId());
        $trick = $tricksRepository->findOneBySlug($slug);

        if ($todelete->getTricks()->getId() === $trick->getId()) {
            $messagesService->deleteMessages($todelete);

            return $this->redirectToRoute('app_show_trick', ['slug' => $slug]);
        }

        $this->addFlash(
            'success',
            'Une erreur c\'est produite lors de la suppression du commentaire'
        );
        
        return $this->redirectToRoute('app_show_trick', ['slug' => $slug]);
    }
}
