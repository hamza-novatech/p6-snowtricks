<?php

namespace App\Controller;

use App\Entity\Tricks;
use App\Entity\TricksAsset;
use App\Entity\TricksPhoto;
use App\Entity\TricksVideo;
use App\Form\TricksAssetPhotoType;
use App\Form\TricksAssetVideoType;
use App\Repository\TricksAssetRepository;
use App\Repository\TricksPhotoRepository;
use App\Repository\TricksRepository;
use App\Service\TricksAssetService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TricksAssetController extends AbstractController
{
    /**
    * @Route("/deleteasset/{id}", methods="GET", name="app_delete_asset")
    */
    public function delete(TricksPhoto $tricksAsset, TricksAssetService $tricksAssetService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $tricksAssetService->deletePhoto($tricksAsset);

        return $this->redirectToRoute('app_show_trick', ['slug' => $tricksAsset->getTrick()->getSlug()]);
    }

    /**
    * @Route("/create/video/{slug}", methods="GET|POST", name="app_create_video_asset")
    */
    public function createVideo(Tricks $tricks, Request $request, TricksAssetService $tricksAssetService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        $asset = $tricksAssetService->newVideo($tricks);

        $form = $this->createForm(TricksAssetVideoType::class, $asset);
        $form->handleRequest($request);

        $tricksAssetService->ifDailymotion($asset);

        if ($form->isSubmitted() && $form->isValid()) {
            $tricksAssetService->addVideo($asset);
        
            return $this->redirectToRoute('app_show_trick', ['slug' => $tricks->getSlug()]);
        }

        return $this->render('tricks_asset/video.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/create/photo/{slug}", methods="GET|POST", name="app_create_photo_asset")
    */
    public function createPhoto(Tricks $tricks, Request $request, TricksPhotoRepository $tricksPhotoRepository, TricksAssetService $tricksAssetService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $mainphoto = $tricksPhotoRepository->findBy(['type' => 'main', 'trick' => $tricks], []);
        
        $asset = $tricksAssetService->newPhoto($tricks, 'photo');

        if (empty($mainphoto)) {
            $asset->setType('main');
        }

        $form = $this->createForm(TricksAssetPhotoType::class, $asset);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tricksAssetService->addPhoto($asset, $form);

            return $this->redirectToRoute('app_show_trick', ['slug' => $tricks->getSlug()]);
        }

        return $this->render('tricks_asset/photo.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/edit/trick/delete/video/{id}", methods="DELETE", name="app_delete_video_asset")
    */
    public function deleteVideo(TricksVideo $tricksVideo, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid('delete'.$tricksVideo->getId(), $data['_token'])) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tricksVideo);
            $em->flush();
    
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }

    /**
    * @Route("/edit/trick/delete/photo/{id}", methods="DELETE", name="app_delete_photo_asset")
    */
    public function deletePhoto(TricksPhoto $tricksPhoto, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($this->isCsrfTokenValid('delete'.$tricksPhoto->getId(), $data['_token'])) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tricksPhoto);
            $em->flush();
    
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }

    /**
    * @Route("/deletevideo/{id}", methods="GET", name="app_delete_video")
    */
    public function deleteVideoFromShow(TricksVideo $tricksVideo, TricksAssetService $tricksAssetService)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $tricksAssetService->deleteVideo($tricksVideo);

        return $this->redirectToRoute('app_show_trick', ['slug' => $tricksVideo->getTrick()->getSlug()]);
    }
}
