# P6 Snowtricks (OpenClassRooms)

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/64921b5c69564128b3a4ef74243602e1)](https://www.codacy.com/manual/yoan.bernabeu/p6-snowtricks?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=yoan.bernabeu/p6-snowtricks&amp;utm_campaign=Badge_Grade)
[![pipeline status](https://gitlab.com/yoan.bernabeu/p6-snowtricks/badges/master/pipeline.svg)](https://gitlab.com/yoan.bernabeu/p6-snowtricks/-/commits/master)

Project as part of my studies with OpenClassrooms. Creation of a social network allowing the sharing of Snowboard figures. Using Symfony.

## Server configuration required

*   MySQL or MariaDB
*   Apache2 (with mod_rewrite activated)
*   Php 7.4
*   Composer
*   git
*   host : Linux Debian or Linux Ubuntu

## Sources and dependencies

*   Copy sources and install dependencies

```bash
sudo git clone https://gitlab.com/yoan.bernabeu/p6-snowtricks.git
sudo composer install
```

## Configuration and Database

*   Modify the .env file with your database information, and your sending mail server
*   Create, Migrate, and, if you want, insert Data Fixtures

```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

* For production, remember to change the value of APP_ENV to "prod" in the .ENV file (or with the environment variables of your server)

## Rights of the upload directory

*   Right to write the directory

```bash
chown -R www-data:www-data public/uploads/photos
chmod -R 770 public/uploads/photos
```

## Apache vhost file configuration

*   Web Server Confuguration ([Official Documntation](https://symfony.com/doc/current/setup/web_server_configuration.html))

```vhost.conf
<VirtualHost *:80>
    ServerName domain.tld
    ServerAlias www.domain.tld

    DocumentRoot /var/www/public
    DirectoryIndex /index.php

    <Directory /var/www/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>

    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>
```

## License

[MIT](https://choosealicense.com/licenses/mit/)