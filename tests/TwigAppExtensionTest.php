<?php

namespace App\Tests;

use App\Twig\AppExtension;
use PHPUnit\Framework\TestCase;

class TwigAppExtensionTest extends TestCase
{
    public function test_Video_Thumb_Youtube_long_url()
    {
        $videoThumb = new AppExtension();
        $this->assertTrue($videoThumb->videoThumb('https://www.youtube.com/watch?v=mffPGWweWJk') === 'https://i3.ytimg.com/vi/mffPGWweWJk/hqdefault.jpg');
        $this->assertFalse($videoThumb->videoThumb('https://www.youtube.com/watch?v=mffPGWweWJk') === 'false');
        $this->assertFalse($videoThumb->videoThumb('false') === 'https://i3.ytimg.com/vi/mffPGWweWJk/hqdefault.jpg');
    }

    public function test_Video_Thumb_Youtube_short_url()
    {
        $videoThumb = new AppExtension();
        $this->assertTrue($videoThumb->videoThumb('https://youtu.be/mffPGWweWJk') === 'https://i3.ytimg.com/vi/mffPGWweWJk/hqdefault.jpg');
        $this->assertFalse($videoThumb->videoThumb('https://youtu.be/mffPGWweWJk') === 'false');
        $this->assertFalse($videoThumb->videoThumb('false') === 'https://i3.ytimg.com/vi/mffPGWweWJk/hqdefault.jpg');
    }

    public function test_Video_Thumb_Vimeo_url()
    {
        $videoThumb = new AppExtension();
        $this->assertTrue($videoThumb->videoThumb('https://vimeo.com/191938432') === 'https://i.vimeocdn.com/video/603132427_200x150.jpg');
        $this->assertFalse($videoThumb->videoThumb('https://vimeo.com/191938432') === 'false');
        $this->assertFalse($videoThumb->videoThumb('false') === 'https://i.vimeocdn.com/video/603132427_200x150.jpg');
    }

    public function test_Video_Thumb_Dailymotion_url()
    {
        $videoThumb = new AppExtension();
        $this->assertTrue($videoThumb->videoThumb('https://www.dailymotion.com/video/x27dc5l') === 'https://www.dailymotion.com/thumbnail/video/x27dc5l');
        $this->assertFalse($videoThumb->videoThumb('https://www.dailymotion.com/video/x27dc5l') === 'false');
        $this->assertFalse($videoThumb->videoThumb('false') === 'https://www.dailymotion.com/thumbnail/video/x27dc5l');
    }

    public function test_Gravatar()
    {
        $gravatar = new AppExtension();
        $this->assertTrue($gravatar->gravatar('test@test.com', 40, 'mp', 'g', true) === '<img src="https://www.gravatar.com/avatar/b642b4217b34b1e8d3bd915fc65c4452?s=40&d=mp&r=g" />');
        $this->assertTrue($gravatar->gravatar('test@test.com') === 'https://www.gravatar.com/avatar/b642b4217b34b1e8d3bd915fc65c4452?s=40&d=mp&r=g');
        $this->assertFalse($gravatar->gravatar('test@test.com') === 'false');
        $this->assertFalse($gravatar->gravatar('false') === 'https://www.gravatar.com/avatar/b642b4217b34b1e8d3bd915fc65c4452?s=40&d=mp&r=g');
    }
}
