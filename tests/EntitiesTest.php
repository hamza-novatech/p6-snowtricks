<?php

namespace App\Tests;

use App\Entity\Message;
use App\Entity\Tricks;
use App\Entity\TricksAsset;
use App\Entity\TricksGroup;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Date;

class EntitiesTest extends TestCase
{
    public function test_tricks_entity_true()
    {
        $trick = new Tricks;
        $user = new User;
        $message = new Message;
        $tricksGroup = new TricksGroup;
        $date_test = new DateTime();
        $trickAsset = new TricksAsset();

        $tricksGroup->setName('test_Group');

        $trick
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreateAt($date_test)
            ->setUpdateAt($date_test)
            ->setSlug('test-slug')
            ->setUser($user)
            ->addMessage($message)
            ->addTricksGroup($tricksGroup)
            ->addTricksAsset($trickAsset)
        ;

        $this->assertTrue($trick->getName() == 'test_name');
        $this->assertTrue(($trick->getDescription() == 'test_description'));
        $this->assertTrue($trick->getSlug() == 'test-slug');
        $this->assertTrue($trick->getUpdateAt() === $date_test);
        $this->assertTrue($trick->getCreateAt() === $date_test);
        $this->assertTrue($trick->getUser() === $user);
        $this->assertContains($message, $trick->getMessages());
        $this->assertContains($tricksGroup, $trick->getTricksGroup());
        $this->assertContains($trickAsset, $trick->getTricksAssets());
    }

    public function test_tricks_entity_false()
    {
        $trick = new Tricks;
        $user = new User;
        $message = new Message;
        $tricksGroup = new TricksGroup;
        $date_test = new DateTime();
        $trickAsset = new TricksAsset();

        $tricksGroup->setName('test_Group');

        $trick
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreateAt($date_test)
            ->setUpdateAt($date_test)
            ->setSlug('test-slug')
            ->setUser($user)
            ->addMessage($message)
            ->addTricksGroup($tricksGroup)
            ->addTricksAsset($trickAsset)
        ;

        $user = "fasle";

        $this->assertFalse($trick->getName() == 'false');
        $this->assertFalse(($trick->getDescription() == 'false'));
        $this->assertFalse($trick->getSlug() == 'false');
        $this->assertFalse($trick->getUpdateAt() === new DateTime());
        $this->assertFalse($trick->getCreateAt() === new DateTime());
        $this->assertFalse($trick->getUser() === $user);
        $this->assertNotContains(new Message, $trick->getMessages());
        $this->assertNotContains(new TricksGroup, $trick->getTricksGroup());
        $this->assertNotContains(new TricksAsset, $trick->getTricksAssets());
    }

    public function test_tricks_entity_empty()
    {
        $trick = new Tricks;

        $this->assertEmpty($trick->getName());
        $this->assertEmpty($trick->getDescription());
        $this->assertEmpty($trick->getSlug());
        $this->assertEmpty($trick->getUpdateAt());
        $this->assertEmpty($trick->getCreateAt());
        $this->assertEmpty($trick->getUser());
        $this->assertEmpty($trick->getMessages());
        $this->assertEmpty($trick->getTricksGroup());
        $this->assertEmpty($trick->getTricksAssets());
        $this->assertEmpty($trick->getId());
    }

    public function test_tricks_entity_remove_action()
    {
        $trick = new Tricks;
        $user = new User;
        $message = new Message;
        $tricksGroup = new TricksGroup;
        $date_test = new DateTime();
        $trickAsset = new TricksAsset();

        $tricksGroup->setName('test_Group');

        $trick
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreateAt($date_test)
            ->setUpdateAt($date_test)
            ->setSlug('test-slug')
            ->setUser($user)
            ->addMessage($message)
            ->addTricksGroup($tricksGroup)
            ->addTricksAsset($trickAsset)
        ;

        $trick
            ->removeMessage($message)
            ->removeTricksAsset($trickAsset)
            ->removeTricksGroup($tricksGroup)
        ;

        $this->assertEmpty($trick->getMessages());
        $this->assertEmpty($trick->getTricksGroup());
        $this->assertEmpty($trick->getTricksAssets());
    }

    public function test_user_entity_true()
    {
        $user = new User;
        $date_test = new DateTime();
        $message = new Message();
        $trick = new Tricks();

        $user
            ->setEmail('test@test.com')
            ->setFirstname('test_firstname')
            ->setLastname('test_lastname')
            ->setPassword('test_password')
            ->setAvatar('test_avatar')
            ->setRegisterOn($date_test)
            ->setResetPassword('reset_password')
            ->addMessage($message)
            ->addTrick($trick)
            ->setRoles(['TEST'])
            ->setIsVerified(true)
        ;

        $this->assertTrue($user->getEmail() == 'test@test.com');
        $this->assertTrue($user->getFirstname() == 'test_firstname');
        $this->assertTrue($user->getLastname() == 'test_lastname');
        $this->assertTrue($user->getPassword() == 'test_password');
        $this->assertTrue($user->getAvatar() == 'test_avatar');
        $this->assertTrue($user->getRegisterOn() == $date_test);
        $this->assertTrue($user->getRoles() === ['TEST', 'ROLE_USER']);
        $this->assertTrue($user->getUsername() == 'test@test.com');
        $this->assertTrue($user->getResetPassword() == 'reset_password');
        $this->assertTrue($user->isVerified());
        $this->assertContains($message, $user->getMessages());
        $this->assertContains($trick, $user->getTricks());
    }

    public function test_user_entity_false()
    {
        $user = new User;
        $date_test = new DateTime();
        $message = new Message();
        $trick = new Tricks();

        $user
            ->setEmail('test@test.com')
            ->setFirstname('test_firstname')
            ->setLastname('test_lastname')
            ->setPassword('test_password')
            ->setAvatar('test_avatar')
            ->setRegisterOn($date_test)
            ->setResetPassword('reset_password')
            ->addMessage($message)
            ->addTrick($trick)
        ;

        $this->assertFalse($user->getEmail() == 'false');
        $this->assertFalse($user->getFirstname() == 'tfalse');
        $this->assertFalse($user->getLastname() == 'false');
        $this->assertFalse($user->getPassword() == 'false');
        $this->assertFalse($user->getAvatar() == 'false');
        $this->assertFalse($user->getRegisterOn() == new DateTime());
        $this->assertFalse($user->getRoles() === ['FALSE']);
        $this->assertFalse($user->getUsername() == 'false');
        $this->assertFalse($user->getResetPassword() == 'false');
        $this->assertFalse($user->isVerified());
        $this->assertNotContains(new Message(), $user->getMessages());
        $this->assertNotContains(new Tricks(), $user->getTricks());
    }

    public function test_user_entity_empty()
    {
        $user = new User;

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAvatar());
        $this->assertEmpty($user->getRegisterOn());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getSalt());
        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getTricks());
        $this->assertEmpty($user->getMessages());
        $this->assertEmpty($user->getResetPassword());
    }

    public function test_user_entity_remove_action()
    {
        $user = new User;
        $date_test = new DateTime();
        $message = new Message();
        $trick = new Tricks();

        $user
            ->setEmail('test@test.com')
            ->setFirstname('test_firstname')
            ->setLastname('test_lastname')
            ->setPassword('test_password')
            ->setAvatar('test_avatar')
            ->setRegisterOn($date_test)
            ->setResetPassword('reset_password')
            ->addMessage($message)
            ->addTrick($trick)
            ->setRoles(['TEST'])
            ->setIsVerified(true)
        ;

        $user
            ->removeMessage($message)
            ->removeTrick($trick)
            ->eraseCredentials()
        ;

        $this->assertEmpty($user->getTricks());
        $this->assertEmpty($user->getMessages());
    }

    public function test_group_entity_true()
    {
        $group = new TricksGroup;
        $date_test = new DateTime();

        $group
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreatedAt($date_test)
        ;

        $this->assertTrue($group->getName() == 'test_name');
        $this->assertTrue($group->getDescription() == 'test_description');
        $this->assertTrue($group->getCreatedAt() == $date_test);
    }

    public function test_group_entity_false()
    {
        $group = new TricksGroup;
        $date_test = new DateTime();

        $group
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreatedAt($date_test)
        ;

        $this->assertFalse($group->getName() == 'false');
        $this->assertFalse($group->getDescription() == 'false');
        $this->assertFalse($group->getCreatedAt() == new DateTime());
    }


    public function test_group_entity_empty()
    {
        $group = new TricksGroup;

        $this->assertEmpty($group->getName());
        $this->assertEmpty($group->getDescription());
        $this->assertEmpty($group->getCreatedAt());
        $this->assertEmpty($group->getId());
        $this->assertEmpty($group->getTricks());
    }

    public function test_group_entity_remove_action()
    {
        $group = new TricksGroup;
        $date_test = new DateTime();
        $trick = new Tricks();

        $group
            ->setName('test_name')
            ->setDescription('test_description')
            ->setCreatedAt($date_test)
            ->addTrick($trick)
        ;

        $group->removeTrick($trick);
        
        $this->assertEmpty($group->getTricks());
    }

    public function test_asset_entity_true()
    {
        $asset = new TricksAsset;
        $trick = new Tricks;
        $date_test = new DateTime();

        $asset
            ->setAsset('test_asset')
            ->setCreateAt($date_test)
            ->setType('test_type')
            ->setTricks($trick)
        ;

        $this->assertTrue($asset->getAsset() == 'test_asset');
        $this->assertTrue($asset->getCreateAt() == $date_test);
        $this->assertTrue($asset->getType() == 'test_type');
        $this->assertTrue($asset->getTricks() == $trick);
    }


    public function test_asset_entity_false()
    {
        $asset = new TricksAsset;
        $trick = new Tricks;
        $date_test = new DateTime();

        $asset
            ->setAsset('test_asset')
            ->setCreateAt($date_test)
            ->setType('test_type')
            ->setTricks($trick)
        ;

        $trick = 'false';

        $this->assertFalse($asset->getAsset() == 'false');
        $this->assertFalse($asset->getCreateAt() == new DateTime());
        $this->assertFalse($asset->getType() == 'false');
        $this->assertFalse($asset->getTricks() == $trick);
    }

    public function test_asset_entity_empty()
    {
        $asset = new TricksAsset;

        $this->assertEmpty($asset->getAsset());
        $this->assertEmpty($asset->getCreateAt());
        $this->assertEmpty($asset->getType());
        $this->assertEmpty($asset->getTricks());
    }

    public function test_message_entity_true()
    {
        $message = new Message;
        $user = new User;
        $tricks = new Tricks;
        $date_test = new DateTime();

        $message
            ->setMessage('test_message')
            ->setCreateAt($date_test)
            ->setUpdateAt($date_test)
            ->setUser($user)
            ->setTricks($tricks)
        ;

        $this->assertTrue($message->getMessage() == 'test_message');
        $this->assertTrue($message->getCreateAt() === $date_test);
        $this->assertTrue($message->getUpdateAt() === $date_test);
        $this->assertTrue($message->getUser() === $user);
        $this->assertTrue($message->getTricks() === $tricks);
    }

    public function test_message_entity_false()
    {
        $message = new Message;
        $user = new User;
        $tricks = new Tricks;
        $date_test = new DateTime();

        $message
            ->setMessage('test_message')
            ->setCreateAt($date_test)
            ->setUpdateAt($date_test)
            ->setUser($user)
            ->setTricks($tricks)
        ;

        $user = 'false';
        $tricks = 'false';

        $this->assertFalse($message->getMessage() == 'false');
        $this->assertFalse($message->getCreateAt() === new DateTime());
        $this->assertFalse($message->getUpdateAt() === new DateTime());
        $this->assertFalse($message->getUser() === $user);
        $this->assertFalse($message->getTricks() === $tricks);
    }

    public function test_message_entity_empty()
    {
        $message = new Message;

        $this->assertEmpty($message->getMessage());
        $this->assertEmpty($message->getCreateAt());
        $this->assertEmpty($message->getUpdateAt());
        $this->assertEmpty($message->getUser());
        $this->assertEmpty($message->getTricks());
        $this->assertEmpty($message->getId());
    }
}
