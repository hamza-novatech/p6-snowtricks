<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ControllerTest extends WebTestCase
{
    public function test_should_see_the_title_SnowTricks_on_the_home_page()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('title', 'SnowTricks');
        $this->assertRouteSame('app_home');
    }

    public function test_should_display_the_details_of_a_trick()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/trick/just-a-fake-slug');

        $this->assertSelectorTextContains('title', 'SnowTricks');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_show_trick');
    }

    public function test_should_display_the_trick_page_from_the_home_page()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/2');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_home');

        $crawler = $client->clickLink('Trick de test');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_show_trick');
        $this->assertSelectorTextContains('body', 'Trick de test');
        $this->assertSelectorTextContains('body', 'message de test');
    }

    public function test_should_allow_us_to_return_to_the_homepage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/trick/just-a-fake-slug');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_show_trick');

        $crawler = $client->clickLink('SnowTricks');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_home');
    }


    public function test_should_display_the_registration_form_from_homepage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_home');

        $crawler = $client->clickLink('Créer un compte');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_register');
    }

    public function test_should_display_the_login_form_from_homepage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_home');

        $crawler = $client->clickLink('Se connecter');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_login');
    }

    public function test_should_display_the_password_reste_form_from_login_page()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_login');

        $crawler = $client->clickLink('J\'ai oublié mon mot de passe');
        $this->assertResponseIsSuccessful();
        $this->assertRouteSame('app_reset_form');
    }
}
