<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622185354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create TricksAsset table';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tricks_asset (id INT AUTO_INCREMENT NOT NULL, tricks_id INT DEFAULT NULL, asset VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, INDEX IDX_DB9F64353B153154 (tricks_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tricks_asset ADD CONSTRAINT FK_DB9F64353B153154 FOREIGN KEY (tricks_id) REFERENCES tricks (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE tricks_asset');
    }
}
