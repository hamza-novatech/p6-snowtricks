<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622184953 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'make ManyToMany for Tricks and TricksGroup';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tricks_tricks_group (tricks_id INT NOT NULL, tricks_group_id INT NOT NULL, INDEX IDX_6ED35BD23B153154 (tricks_id), INDEX IDX_6ED35BD2DE4E02E0 (tricks_group_id), PRIMARY KEY(tricks_id, tricks_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tricks_tricks_group ADD CONSTRAINT FK_6ED35BD23B153154 FOREIGN KEY (tricks_id) REFERENCES tricks (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tricks_tricks_group ADD CONSTRAINT FK_6ED35BD2DE4E02E0 FOREIGN KEY (tricks_group_id) REFERENCES tricks_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE tricks_tricks_group');
    }
}
